#!/bin/bash
# NOTE: use a whole path for zip files
# Sometimes the /tmp/confluence/ subdirs input/ and workspace/ go away which can cause the script to fail silently
rm -rf /tmp/confluence/input/* /tmp/confluence/workspace/* /tmp/result/
unzip $1 -d /tmp/confluence/input/
/srv/migrate-confluence/bin/migrate-confluence analyze --src /tmp/confluence/input/ --dest /tmp/confluence/workspace/
/srv/migrate-confluence/bin/migrate-confluence extract --src /tmp/confluence/input/ --dest /tmp/confluence/workspace/
/srv/migrate-confluence/bin/migrate-confluence convert --src /tmp/confluence/workspace/ --dest /tmp/confluence/workspace/
/srv/migrate-confluence/bin/migrate-confluence compose --src /tmp/confluence/workspace/ --dest /tmp/confluence/workspace/
cp -r /tmp/confluence/workspace/result /tmp
php /var/lib/mediawiki/maintenance/importImages.php /tmp/result/images/
php /var/lib/mediawiki/maintenance/importDump.php /tmp/result/output.xml
php /var/lib/mediawiki/maintenance/rebuildrecentchanges.php
php /var/lib/mediawiki/maintenance/initSiteStats.php
php /var/lib/mediawiki/maintenance/update.php

echo "---"
prefix=`grep '=>' /tmp/confluence/workspace/space-name-to-prefix-map.php | cut -d'>' -f 2 | cut -c 3- | rev | cut -c 3- | rev`
echo "URLs for the space for the stem $prefix:"
echo "https://sandbox.wiki.duke.edu/index.php/Special:PrefixIndex?prefix=$prefix"
echo "https://sandbox.wiki.duke.edu/index.php/$prefix:Main_Page"
#echo "The namespace prefix is on the right-hand side of => :"
#cat /tmp/confluence/workspace/space-name-to-prefix-map.php
